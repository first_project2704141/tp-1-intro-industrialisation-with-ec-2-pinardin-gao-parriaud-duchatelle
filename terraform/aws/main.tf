# PROVIDER
provider "aws" {
  region = var.region
}

resource "aws_key_pair" "admin" {
  key_name   = "admin2"
  public_key = file(var.aws_public_key_ssh_path)
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "admin"

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.admin]
}

resource "aws_dynamodb_table" "example" {
  name           = "example-table"
  billing_mode   = "PAY_PER_REQUEST" # Use "PROVISIONED" for provisioned capacity
  hash_key       = "id" # Change to your desired hash key attribute
  attribute {
    name = "id"
    type = "S" # "S" for string, "N" for number, etc.
  }
  tags = {
    Name = "example-table"
  }
}

resource "aws_iam_policy" "example_policy" {
  name        = "example-policy"
  description = "Example IAM policy"
  
  # Define the policy document here
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "s3:GetObject",
        Effect   = "Allow",
        Resource = "*"
      }
    ]
  })
}


resource "aws_iam_role" "example_role" {
  name = "example-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
      },
    ],
  })
}

resource "aws_iam_instance_profile" "example_instance_profile" {
  name = "example-instance-profile"
  role = aws_iam_role.example_role.name
}

resource "aws_iam_policy_attachment" "dynamodb_access_attachment" {
  name       = "dynamodb-access-attachment"
  roles      = [aws_iam_role.example_role.name]
  policy_arn = aws_iam_policy.dynamodb_full_access_policy.arn
}



resource "aws_iam_policy" "dynamodb_full_access_policy" {
  name        = "DynamoDBFullAccessPolicy"
  description = "IAM policy for full access to DynamoDB"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "dynamodb:*",
        Effect = "Allow",
        Resource = "*",
      },
    ],
  })
}
