def division(num, denum) -> int:
    if denum == 0:
        raise ZeroDivisionError("Denominator (denum) cannot be zero.")
    return num // denum
